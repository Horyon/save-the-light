﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private Animator _anim;
    private Rigidbody _controller;

    private float _xAxis;
    private float _zAxis;
    private Vector3 moveVector;

    public float speed = 4;
    public float jumpSpeed = 8;

    void Awake()
    {
        _anim = GetComponent<Animator>();
        _controller = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _xAxis = Input.GetAxis("Horizontal");
        _zAxis = Input.GetAxis("Vertical");
        
        moveVector = new Vector3(_xAxis, 0, _zAxis);
        moveVector *= Time.deltaTime * speed;
        
        Movement(_xAxis, _zAxis);
    }

    void Movement( float x, float z)
    {
        _anim.SetFloat("InputX", x);
        _anim.SetFloat("InputZ", z);

        _controller.transform.Translate(moveVector);
    }
}