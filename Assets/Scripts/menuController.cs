﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuController : MonoBehaviour
{
    public string text;

    public void onStartGame(){
        SceneManager.LoadScene("SampleLevel");
    }

    public void onQuitGame(){
        Application.Quit();
    }
}